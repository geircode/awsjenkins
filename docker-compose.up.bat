cd %~dp0
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
pause
docker exec -it awsconfig_awscli_1 /bin/bash