# A list of commands I used during the Jenkins setup:


aws iam create-access-key --user-name geircode
aws configure

aws cloudformation create-stack --template-body file://ecs-cluster.template --stack-name EcsClusterStack --capabilities CAPABILITY_IAM --tags Key=Name,Value=ECS --region eu-west-1 --parameters ParameterKey=KeyName,ParameterValue=geircode-keypair-001 ParameterKey=EcsCluster,ParameterValue=getting-started ParameterKey=AsgMaxSize,ParameterValue=2
aws cloudformation describe-stacks --stack-name EcsClusterStack --query 'Stacks[*].[StackId, StackStatus]'

# aws cloudformation create-stack --template-body file://jenkins/ecs-jenkins-demo.template --stack-name JenkinsStack --capabilities CAPABILITY_IAM --tags Key=Name,Value=Jenkins --region eu-west-1 --parameters ParameterKey=EcsStackName,ParameterValue=EcsClusterStack
aws cloudformation create-stack --template-body file://jenkins/ecs-jenkins-geircode.template --stack-name JenkinsStack --capabilities CAPABILITY_IAM --tags Key=Name,Value=Jenkins --region eu-west-1 --parameters ParameterKey=EcsStackName,ParameterValue=EcsClusterStack
aws cloudformation update-stack --template-body file://jenkins/ecs-jenkins-geircode.template --stack-name JenkinsStack --capabilities CAPABILITY_IAM --tags Key=Name,Value=Jenkins --region eu-west-1 --parameters ParameterKey=EcsStackName,ParameterValue=EcsClusterStack
aws cloudformation describe-stacks --stack-name JenkinsStack --query 'Stacks[*].[StackId, StackStatus]'

aws ec2 describe-instances --filters "Name=tag-value","Values=JenkinsStack" --region eu-west-1 | jq .Reservations[].Instances[].PublicDnsName

cp geircode-keypair-001.pem ~
chmod 0400 ~/geircode-keypair-001.pem
ssh -i ~/geircode-keypair-001.pem ec2-user@ec2-52-50-154-61.eu-west-1.compute.amazonaws.com
ssh -i ~/geircode-keypair-001.pem ec2-user@ec2-34-242-97-26.eu-west-1.compute.amazonaws.com
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

aws ecr create-repository --repository-name hello-world --region eu-west-1
# tag-001
# https://forums.docker.com/t/how-can-i-run-docker-command-inside-a-docker-container/337/9
# RUN curl -fsSLO https://get.docker.com/builds/Linux/x86_64/docker-17.03.1-ce.tgz
# tar --strip-components=1 -xvzf docker-17.03.1-ce.tgz -C /usr/local/bin
# $(aws ecr get-login --no-include-email)
# $(aws ecr get-login --region eu-west-1 --no-include-email)

# tag-002

ssh-keygen -t rsa -b 4096 -C geircode@geircode.no

scp -i ~/geircode-keypair-001.pem ec2-user@ec2-34-242-97-26.eu-west-1.compute.amazonaws.com:/var/lib/jenkins/jobs/MyFreestyleProject.zip /app/MyFreestyleProject.zip
cd /var/lib/jenkins/jobs/
# kopier MyFreestyleProject.zip til ny Jenkins server, men først må "/var/lib/jenkins/jobs" katalogen få chmod "777"
ssh -t -i ~/geircode-keypair-001.pem ec2-user@ec2-34-242-97-26.eu-west-1.compute.amazonaws.com sudo chmod 777 /var/lib/jenkins/jobs
scp -i ~/geircode-keypair-001.pem /app/MyFreestyleProject.zip ec2-user@ec2-34-242-97-26.eu-west-1.compute.amazonaws.com:/var/lib/jenkins/jobs/MyFreestyleProject.zip
ssh -t -i ~/geircode-keypair-001.pem ec2-user@ec2-34-242-97-26.eu-west-1.compute.amazonaws.com sudo unzip /var/lib/jenkins/jobs/MyFreestyleProject.zip
# Restart Jenkins for at den skal oppdage det kopierte prosjektet.